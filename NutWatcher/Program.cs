﻿using System;
using System.IO;
using System.Collections.Generic;
using MArchiveBatchTool;
using MArchiveBatchTool.MArchive;
using MArchiveBatchTool.Psb;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using McMaster.Extensions.CommandLineUtils;

namespace NutWatcher
{
    class Program
    {
        static readonly List<string> TARGET_EXTENSIONS = new List<string> { ".json", ".nut", ".bin" };
        static readonly string[] AVAILABLE_CODECS = { "zstd", "zstandard", "zlib" };

        static FileSystemWatcher watcher;
        static string inputPath;
        static string outputPath;
        static MArchivePacker packer;

        static int Main(string[] args)
        {
            var app = new CommandLineApplication
            {
                Name = Path.GetFileName(Environment.GetCommandLineArgs()[0]),
                FullName = "M2 File Watcher"
            };

            var inputPathArg = app.Argument("inputPath", "The input folder to watch").IsRequired();
            inputPathArg.Accepts().ExistingDirectory();
            var outputPathArg = app.Argument("outputPath", "The folder to output packed files to").IsRequired();
            var codecArg = app.Argument("codec", "The codec used for packing/unpacking").IsRequired();
            codecArg.Accepts().Values(AVAILABLE_CODECS);
            var keyArg = app.Argument("seed", "The static seed").IsRequired();
            var keyLengthArg = app.Argument<int>("keyLength", "The key cycle length").IsRequired();

            app.HelpOption();

            app.OnExecute(() =>
            {
                inputPath = Path.GetFullPath(inputPathArg.Value);
                outputPath = Path.GetFullPath(outputPathArg.Value);
                packer = new MArchivePacker(GetCodec(codecArg.Value), keyArg.Value, keyLengthArg.ParsedValue);

                RunWatcher();
            });

            try
            {
                return app.Execute(args);
            }
            catch (CommandParsingException ex)
            {
                Console.Error.WriteLine(ex.Message);
                return 1;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error while processing: {0}", ex);
                return -1;
            }
        }

        static IMArchiveCodec GetCodec(string name)
        {
            switch (name)
            {
                case "zlib":
                    return new ZlibCodec();
                case "zstd":
                case "zstandard":
                    return new ZStandardCodec();
                default:
                    throw new ArgumentException("Unknown codec name", nameof(name));
            }
        }

        static void RunWatcher()
        {
            using (watcher = new FileSystemWatcher())
            {
                watcher.Path = inputPath;
                watcher.NotifyFilter = NotifyFilters.LastWrite
                    | NotifyFilters.CreationTime
                    | NotifyFilters.FileName
                    | NotifyFilters.DirectoryName;
                watcher.Filter = "*";
                watcher.IncludeSubdirectories = true;
                watcher.Changed += OnChanged;
                watcher.Created += OnChanged;
                watcher.Deleted += OnChanged;
                watcher.Renamed += OnRenamed;

                watcher.EnableRaisingEvents = true;
                Console.WriteLine("Running; press q to quit");
                while (Console.ReadKey().KeyChar != 'q') ;
                watcher.EnableRaisingEvents = false;
            }
        }

        static bool IsTargeted(string path)
        {
            return TARGET_EXTENSIONS.Contains(Path.GetExtension(path).ToLower());
        }

        private static void OnRenamed(object sender, RenamedEventArgs e)
        {
            if (!IsTargeted(e.OldName) && !IsTargeted(e.Name)) return;
            Console.WriteLine($"Deleting {e.OldFullPath.Replace(inputPath, string.Empty).TrimStart('/', '\\')}");
            File.Delete(e.OldFullPath.Replace(inputPath, outputPath));
        }

        private static void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (!IsTargeted(e.Name)) return;
            string extension = Path.GetExtension(e.Name).ToLower();
            Console.WriteLine($"Updating {e.FullPath.Replace(inputPath, string.Empty).TrimStart('/', '\\')}");
            System.Threading.Thread.Sleep(1000); // Wait a bit for write to finish
            try
            {
                if (extension == ".json")
                {
                    SerializePsb(e.FullPath, 3, PsbFlags.None, null, true, false);
                    if (packer.NoCompressionFilters.Contains(Path.GetFileName(Path.GetDirectoryName(e.FullPath)).ToLower()))
                    {
                        string srcPath = Path.ChangeExtension(e.FullPath, null);
                        string targetPath = srcPath.Replace(inputPath, outputPath);
                        File.Delete(targetPath);
                        Directory.CreateDirectory(Path.GetDirectoryName(targetPath));
                        File.Move(srcPath, targetPath);
                    }
                    else
                    {
                        packer.CompressFile(Path.ChangeExtension(e.FullPath, null));
                        string mPath = Path.ChangeExtension(e.FullPath, ".m");
                        string targetPath = mPath.Replace(inputPath, outputPath);
                        File.Delete(targetPath);
                        Directory.CreateDirectory(Path.GetDirectoryName(targetPath));
                        File.Move(mPath, targetPath);
                    }
                }
                else
                {
                    packer.CompressFile(e.FullPath, true);
                    string mPath = e.FullPath + ".m";
                    string targetPath = mPath.Replace(inputPath, outputPath);
                    File.Delete(targetPath);
                    Directory.CreateDirectory(Path.GetDirectoryName(targetPath));
                    File.Move(mPath, targetPath);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while processing file: " + ex);
            }
        }

        static void SerializePsb(string path, ushort version, PsbFlags flags, IPsbFilter filter, bool optimize, bool readAsFloat)
        {
            string psbPath = Path.ChangeExtension(path, null);
            if (!psbPath.ToLower().EndsWith(".psb")) psbPath += ".psb";
            using (StreamReader reader = File.OpenText(path))
            using (Stream writer = File.Create(psbPath))
            {
                JsonTextReader jReader = new JsonTextReader(reader)
                {
                    FloatParseHandling = readAsFloat ? FloatParseHandling.Double : FloatParseHandling.Decimal
                };
                JToken root = JToken.ReadFrom(jReader);
                IPsbStreamSource streamSource = new CliStreamSource(Path.ChangeExtension(path, ".streams"));
                PsbWriter psbWriter = new PsbWriter(root, streamSource);
                psbWriter.Version = version;
                psbWriter.Flags = flags;
                psbWriter.Optimize = optimize;
                psbWriter.Write(writer, filter);
            }
        }
    }
}
